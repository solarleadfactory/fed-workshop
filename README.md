# SLF FED Workshop #

### Goal of this workshop ###

The goal of this workshop is to get an overview of your technical skills.

### Your Task ###

You will need to create a working prototype of a simple page (using React) which has a popup or lightbox. You can find the popup layout in the resources directory, and a reference `index.html` file to use as a base page.

### Functionality ###

* After page load, popup should show on the center of the screen. 
* If screen size is under 800px right pane should move to the top. See `resources/mobile-layout.png`.
* Installer list is dynamic and it should be loaded from `/assets/data/contact-list.json` file.
* In the top right corner please add a close button. On click it should hide the popup.
* Add an AB testing which changes the popup header ("Your home is perfect for solar!") on 33%/33%/33% of traffic. 
* Header is dynamic and you should read data from `/assets/data/ab-testing.json`.
* Bonus: Add a countup animation to the $00.00 area similar to this js [example](https://www.jqueryscript.net/demo/Smooth-Animated-Numbers-with-Javascript-CSS3-odometer/test/).
* Bonus: add transitions for opening/closing the popup.

### Requirements ###

* Page has to work in all modern browsers.
* Make sure it looks good on all device versions.
* Please create clean and readable code.   
* You can organize the CSS in the best way that you consider (+ points if you use Tailwind CSS).

### After you finished coding ###

* Please test your code in various browsers. 
* If it works then compress it and send it to us (no need to add the dependencies folder).

Good Luck!
